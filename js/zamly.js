(function () {
  'use strict';

  angular
    .module('zamly', [
      'zamly.config',
      'zamly.routes',
      'zamly.layout',
      'zamly.utils',
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngRoute',
      'adaptive',
    ]);

  angular
    .module('zamly.config', []);

  angular
    .module('zamly.routes', ['ngRoute','ngSanitize']);

  angular
    .module('zamly')
    .run(run);

  run.$inject = ['$http'];

  /**
   * @name run
   * @desc Update xsrf $http headers to align with Django's defaults
   */
  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }
  /*run.$inject = ['$http'];

  /**
   * @name run
   * @desc Update xsrf $http headers to align with Django's defaults
   */
  /*function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }*/
})();