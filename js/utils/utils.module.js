(function () {
  'use strict';

  angular
    .module('zamly.utils', [
      'zamly.utils.services'
    ]);

  angular
    .module('zamly.utils.services', []);
})();
