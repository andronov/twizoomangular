(function () {
  'use strict';

  angular
    .module('zamly.routes')
    .config(config);

  config.$inject = ['$routeProvider'];

  /**
   * @name config
   * @desc Define valid application routes
   */
  function config($routeProvider) {
    $routeProvider.when('/', {
      controller: 'IndexController', 
      controllerAs: 'vm',
      templateUrl: '/templates/layout/index.html?_=' + Math.random()
    }).when('/:hastagID/top', {
      controller: 'TopController', 
      templateUrl: '/templates/layout/top.html?_=' + Math.random()
    }).when('/w/:hastagID/top', {
      controller: 'TopWordController', 
      templateUrl: '/templates/layout/top.html?_=' + Math.random()
    }).when('/status/:statusID', {
      controller: 'TweetController', 
      templateUrl: '/templates/layout/tweet.html?_=' + Math.random()
    }).when('/r/:statusID', {
      controller: 'UpdateController', 
      templateUrl: '/templates/layout/update.html?_=' + Math.random()
    }).otherwise('/');
  }
})();