/**
 * IndexController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('MainController', MainController);

  MainController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope"];

  /**
   * @namespace MainController
   */
  function MainController($scope, $sce, $routeParams, $route, $http, $location, $rootScope)   {
    $scope.linkstatus;
    
    $scope.saveData = function(linkstatus) {
       console.log(linkstatus);
    };
  }
})();