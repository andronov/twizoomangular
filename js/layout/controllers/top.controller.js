/**
 * TopController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('TopController', TopController);

  TopController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope", "TopHash", "TopHashCount"];

  /**
   * @namespace TopController
   */
  function TopController($scope, $sce, $routeParams, $route, $http, $location, $rootScope, TopHash, TopHashCount)   {
    $scope.loading = true;
    $scope.result = TopHash.get({hastagID: $routeParams.hastagID}, function(tweet) {
      $scope.tweet = $scope.result.object;
      $scope.tweets = $scope.result.tweets;
    });

    $scope.getTop = function(count) {
      var countID = count.tweets.length + 10;
      if(countID==100){$('.top__more').hide()}
      $scope.results = TopHashCount.get({hastagID: $routeParams.hastagID,countID:countID}, function(tweet) {
        $scope.tweet = $scope.results.object;
        $scope.tweets = $scope.results.tweets;
      });
    };

  }
})();