/**
 * UpdateController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('UpdateController', UpdateController);

  UpdateController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope", "Updates"];

  /**
   * @namespace UpdateController
   */
  function UpdateController($scope, $sce, $routeParams, $route, $http, $location, $rootScope, Updates)   {
    console.log('yes');
    $scope.result = Updates.get({statusID: $routeParams.statusID}, function(tweet) {
      $scope.tweet = $scope.result.object;
      $scope.tweets = $scope.result.tweets;
    });


    

  }
})();