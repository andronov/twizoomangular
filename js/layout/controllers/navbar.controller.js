/**
 * NavbarController
 * @namespace zamly.layout.controllers
 */
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope"];

  /**
   * @namespace NavbarController
   */
  function NavbarController($scope, $sce, $routeParams, $route, $http, $location, $rootScope)   {
    $scope.$on("$routeChangeSuccess", function(ev, current, previous) {
      $scope.mainNavi.length || $scope.getMainNavi()
    }), $scope.loadingMainNavi = !0, $scope.mainNavi = [], $scope.getMainNavi = function() {
      $scope.loadingMainNavi = !0, $http({
        withCredentials: true,url: "/api/v1/trends/&format=json",
        method: "GET"
      }).success(function(response) {
        if (response) {
          $scope.mainNavi = response; console.log(response);
        } else alert("Произошла ошибка. Пожалуйста, перезагрузите страницу")
      })
    },accordion()
  }
})();

function accordion(){
  $('#accordion > li ul')
    .click(function(event){
      event.stopPropagation();
    })
    .filter(':not(:first)')
    .hide(); 
    
  $('#accordion > li, #accordion > li > ul > li').click(function(){
    var selfClick = $(this).find('ul:first').is(':visible');
    if(!selfClick) {
      $(this)
        .parent()
        .find('> li ul:visible')
        .slideToggle();
    }
    
    $(this)
      .find('ul:first')
      .stop(true, true)
      .slideToggle();
  });
}