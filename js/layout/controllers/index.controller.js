/**
 * IndexController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope"];

  /**
   * @namespace IndexController
   */
  function IndexController($scope, $sce, $routeParams, $route, $http, $location, $rootScope)   {
    $scope.$on("$routeChangeSuccess", function(ev, current, previous) {
      $scope.MainTweets.length || $scope.getMainTweets()
    }), $scope.loadingMainTweets = !0, $scope.MainTweets = [], $scope.getMainTweets = function() {
      $scope.loadingMainTweets = !0, $http({
        withCredentials: true,url: "/api/v1/tweets/&format=json",
        method: "GET"
      }).success(function(response) {
        if (response) {
          $scope.MainTweets = response; console.log(response);
        } else alert("Произошла ошибка. Пожалуйста, перезагрузите страницу")
      })
    }
  }
})();