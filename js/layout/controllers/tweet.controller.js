/**
 * TweetController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('TweetController', TweetController);

  TweetController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope", "Tweet"];

  /**
   * @namespace TweetController
   */
  function TweetController($scope, $sce, $routeParams, $route, $http, $location, $rootScope, Tweet)   {
    $scope.result = Tweet.get({statusID: $routeParams.statusID}, function(tweet) {
      console.log($scope.result);
      $scope.tweet = $scope.result.object;
      $scope.tweets = $scope.result.tweets;
    });


    

  }
})();