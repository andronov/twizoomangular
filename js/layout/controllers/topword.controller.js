/**
 * TopWordController
 * @namespace zamly.layout.controllers
 */

 
(function () {
  'use strict';

  angular
    .module('zamly.layout.controllers')
    .controller('TopWordController', TopWordController);

  TopWordController.$inject = ["$scope",'$sce', "$routeParams", "$route", "$http", "$location", "$rootScope", "TopHashW", "TopHashCountW"];

  /**
   * @namespace TopWordController
   */
  function TopWordController($scope, $sce, $routeParams, $route, $http, $location, $rootScope, TopHashW, TopHashCountW)   {
    $scope.loading = true;
    $scope.result = TopHashW.get({hastagID: $routeParams.hastagID}, function(tweet) {
      $scope.tweet = $scope.result.object;
      $scope.tweets = $scope.result.tweets;
    });

    $scope.getTop = function(count) {
      var countID = count.tweets.length + 10;
      if(countID==100){$('.top__more').hide()}
      $scope.results = TopHashCountW.get({hastagID: $routeParams.hastagID,countID:countID}, function(tweet) {
        $scope.tweet = $scope.results.object;
        $scope.tweets = $scope.results.tweets;
      });
    };

  }
})();