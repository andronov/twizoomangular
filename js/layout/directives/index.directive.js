/**
 * Posts
 * @namespace zamly.layout.directives
 */
(function () {
  'use strict';

  angular
    .module('zamly.layout.directives')
    .directive('toHtml', function() {
       return {
          restrict: 'A',
          link: function (scope, el, attrs) {
            console.log(el.html());
          el.html(scope.$eval(attrs.toHtml));
       }
     };
    })
    .directive('fancybox',function(){
    return {
        link: function(scope, element, attrs) {
            $('.fancybox').fancybox({
        prevEffect  : 'none',
        nextEffect  : 'none',
        padding     : 0,
        helpers : {
            title   : {
                type: 'outside'
            },
            thumbs  : {
                width   : 50,
                height  : 50
            }
        }
    });
        }
    }
    })
    .directive('sociallocker',function(){
    return {
          link: function(scope, element, attrs) {
            console.log(element);
            scope.$watch(attrs.words, function(value) {
                    console.log('element=',value)
                    });
            $('.tweets__block .to-lock').sociallocker({
              buttons: {order:["twitter-tweet"]},
              twitter: {text:"Top 10 tweets #nhl Trend. Learn more at"},
              text: {
                 header: "  ",
                 message: "  "//To view the Top 10, share the link.
              },
              locker: {close: false, timer: 0,},
              theme: 'horizontal'
            });
          }
        }
    })
    .directive("masonry", function($parse) {
    return {
      restrict: 'AC',
      controller:function($scope,$element){
        var bricks = [];
        this.addBrick = function(brick){
          bricks.push(brick)
        }
        this.removeBrick = function(brick){
          var index = bricks.indexOf(brick);
          if(index!=-1)bricks.splice(index,1);
        }
        $scope.$watch(function(){
          return bricks
        },function(){
          console.log('reload');
          $element.imagesLoaded(function(){
            $element.masonry('reloadItems');
            if(bricks){$('.masonry').masonry({columnWidth: 50,itemSelector: '.masonry-brick',isAnimated: false});}
          });
        },true);

        
      },
      link: function (scope, elem, attrs) {
        elem.masonry({columnWidth: 50,itemSelector: '.masonry-brick',isAnimated: false});
      }
    };     
    })
    .directive('masonryBrick', function ($compile) {
    return {
      restrict: 'AC',
      require:'^masonry',
      link: function (scope, elem, attrs,ctrl) {
        //console.log(elem);
        ctrl.addBrick(scope.$id);
        
        scope.$on('$destroy',function(){
          console.log('destroy');
          ctrl.removeBrick(scope.$id);
        });
      }
    };
    })
})();