/**
 * Posts
 * @namespace zamly.layout.directives
 */
(function () {
  'use strict';

  angular
    .module('zamly.layout.directives')
    .directive('layout', posts);

  
})();