(function () {
  'use strict';

  angular
    .module('zamly.layout', [
      'zamly.layout.services',
      'zamly.layout.controllers',
      'zamly.layout.directives',
    ]);

  angular
    .module('zamly.layout.services', ['ngResource', 'ngRoute']);  

  angular
    .module('zamly.layout.controllers', []);

  angular
    .module('zamly.layout.directives', ['ngDialog']);

  
})();