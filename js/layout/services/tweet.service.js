(function () {

  'use strict';

  var tweetServices = angular.module('zamly.layout.services', ['ngResource']);

  tweetServices.factory('Tweet', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/:statusID/?format=json', {}, {
        query: {method:'GET', params:{statusID:'statusID'}, isArray:true}
      });//list tweets
    }]).factory('TopHash', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/top/:hastagID/?format=json', {}, {
        query: {method:'GET', params:{hastagID:'hastagID'}, isArray:true}
      });//Top tweets
    }]).factory('TopHashCount', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/top/:hastagID/?format=json&count=:countID', {}, {
        query: {method:'GET', params:{hastagID:'hastagID', countID:'countID'}, isArray:true}
      });//Top tweets count
    }]).factory('TopHashW', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/top/w/:hastagID/?format=json', {}, {
        query: {method:'GET', params:{hastagID:'hastagID'}, isArray:true}
      });//Top tweets words
    }]).factory('TopHashCountW', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/top/w/:hastagID/?format=json&count=:countID', {}, {
        query: {method:'GET', params:{hastagID:'hastagID', countID:'countID'}, isArray:true}
      });//Top tweets count words
    }]).factory('Updates', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/updates/:statusID/?format=json', {}, {
        query: {method:'GET', params:{statusID:'statusID'}, isArray:true}
      });
    }]);//updates

})();