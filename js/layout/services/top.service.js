(function () {

  'use strict';

  var topHashServices = angular.module('zamly.layout.services', ['ngResource']);

  topHashServices.factory('TopHash', ['$resource',
    function($resource){
      return $resource('/api/v1/tweets/top/:hastagID/?format=json', {}, {
        query: {method:'GET', params:{hastagID:'hastagID'}, isArray:true}
      });
    }]);
})();